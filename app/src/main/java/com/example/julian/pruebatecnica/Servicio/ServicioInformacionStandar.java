package com.example.julian.pruebatecnica.Servicio;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.example.julian.pruebatecnica.Aplicacion;
import com.example.julian.pruebatecnica.BD.Constantes.ConstanteStandar;
import com.example.julian.pruebatecnica.BD.SqlLiteHelper;
import com.example.julian.pruebatecnica.BD.TablasHelper;
import com.example.julian.pruebatecnica.Modelo.ModeloStandar;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

public class ServicioInformacionStandar {

    @Inject
    SqlLiteHelper sqlLiteHelper;

    SQLiteDatabase db;

    public ServicioInformacionStandar() {
        Aplicacion.getApp().getComponente().inject(this);
        db = sqlLiteHelper.getReadableDatabase();
    }

    public List<ModeloStandar> getInformacionStandar(String nomTabla) {
        Cursor cStandar = TablasHelper.getDatos(db, nomTabla);
        List<ModeloStandar> modeloStandarList = new ArrayList<ModeloStandar>();
        if (cStandar.moveToFirst()) {
            for(cStandar.moveToFirst(); !cStandar.isAfterLast(); cStandar.moveToNext()) {
                ModeloStandar modeloStandar = new ModeloStandar();
                modeloStandar.setId(cStandar.getInt(0));
                modeloStandar.setNombre(cStandar.getString(1));
                modeloStandarList.add(modeloStandar);
            }
        }

        return modeloStandarList;
    }

    public List<ModeloStandar> getInformacionSede(Integer id) {
        Cursor sede = TablasHelper.getDatos(db, ConstanteStandar.TABLE_SEDE);
        List<ModeloStandar> modeloStandarList = new ArrayList<>();
        if (sede.moveToFirst()) {
            do {
                if (id.equals(sede.getInt(2))) {
                    ModeloStandar modeloStandar = new ModeloStandar();
                    modeloStandar.setId(sede.getInt(0));
                    modeloStandar.setNombre(sede.getString(1));
                    modeloStandar.setIdEstablecimiento(sede.getInt(2));
                    modeloStandarList.add(modeloStandar);
                }

            } while (sede.moveToNext());
        }

        return modeloStandarList;
    }
}
