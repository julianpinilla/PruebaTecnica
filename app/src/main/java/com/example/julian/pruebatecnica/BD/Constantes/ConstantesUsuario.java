package com.example.julian.pruebatecnica.BD.Constantes;

public final class ConstantesUsuario {

    /*Nombre de la tabla Usuario*/
    public static final String TABLE_NAME = "usuario";

    /*Nombre Columna Tabla Usuario*/
    public static final String COL_ID = "id";
    public static final String COL_USER = "user";
    public static final String COL_PASS = "password";


}
