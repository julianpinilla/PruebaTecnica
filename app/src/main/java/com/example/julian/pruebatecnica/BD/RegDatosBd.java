package com.example.julian.pruebatecnica.BD;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.example.julian.pruebatecnica.Aplicacion;
import com.example.julian.pruebatecnica.BD.Constantes.ConstanteStandar;
import com.example.julian.pruebatecnica.BD.Constantes.ConstantesEstudiante;
import com.example.julian.pruebatecnica.BD.Constantes.ConstantesUsuario;
import com.example.julian.pruebatecnica.Utils.Utils;

import java.io.BufferedReader;
import java.io.IOException;

import javax.inject.Inject;

import static com.example.julian.pruebatecnica.Utils.Utils.addComilla;

public class RegDatosBd {

    private static final String FILE_USUARIO = "Usuario.csv";
    private static final String FILE_ESTABLECIMIENTO = "Establecimiento.csv";
    private static final String FILE_SEDE = "Sede.csv";
    private static final String FILE_JORNADA = "Jornada.csv";
    private static final String FILE_GRUPO = "Grupo.csv";
    private static final String FILE_GRADO = "Grado.csv";
    private static final String FILE_ESTUDIANTE = "estudiante.csv";
    private Context context;

    @Inject
    SqlLiteHelper sqlLiteHelper;

    SQLiteDatabase db;

    public RegDatosBd(Context context) {
        Aplicacion.getApp().getComponente().inject(this);
        this.context = context;
        db = sqlLiteHelper.getWritableDatabase();
    }

    /**
     * Meotodo encargado de llamar los metodos de carga de datos
     */
    public void regInformacion() {
        regUsuario();
        regDatosStandar(ConstanteStandar.TABLE_ESTABLECIMIENTO, FILE_ESTABLECIMIENTO);
        regDatosStandar(ConstanteStandar.TABLE_SEDE, FILE_SEDE);
        regDatosStandar(ConstanteStandar.TABLE_JORNADA, FILE_JORNADA);
        regDatosStandar(ConstanteStandar.TABLE_GRUPO, FILE_GRUPO);
        regDatosStandar(ConstanteStandar.TABLE_GRADO, FILE_GRADO);
        regEstudiante();
    }

    /**
     * metodo encargado de leer el csv del usuario y cargarlo en la bd
     */
    private void regUsuario() {

        if (!validaDatos(ConstantesUsuario.TABLE_NAME)) {
            return;
        }
        try {
            BufferedReader reader = Utils.getDatosFile(context, FILE_USUARIO);
            String line = "";
            while ((line = reader.readLine()) != null) {
                String[] str = line.split(";");
                StringBuilder sb = new StringBuilder(TablasHelper.INSERT);
                sb.append(ConstantesUsuario.TABLE_NAME);
                sb.append(TablasHelper.RIGHT_PARENTESIS);
                sb.append(ConstantesUsuario.COL_ID);
                sb.append(TablasHelper.COMMA);
                sb.append(ConstantesUsuario.COL_USER);
                sb.append(TablasHelper.COMMA);
                sb.append(ConstantesUsuario.COL_PASS);
                sb.append(TablasHelper.LEFT_PARENTESIS);
                sb.append(TablasHelper.VALUE);
                sb.append(TablasHelper.RIGHT_PARENTESIS);
                sb.append(str[0]);
                sb.append(TablasHelper.COMMA);
                sb.append(addComilla(str[1]));
                sb.append(TablasHelper.COMMA);
                sb.append(addComilla(str[2]));
                sb.append(TablasHelper.LEFT_PARENTESIS);
                db.execSQL(sb.toString());
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * metodo encargado de leer el csv de entidades, jornada, grupo y grado  y registrarlo en la BD corresponiente
     * @param nomTabla
     * @param file
     */
    private void regDatosStandar(String nomTabla, String file) {

        if (!validaDatos(nomTabla)) {
            return;
        }

        try {
            BufferedReader reader = Utils.getDatosFile(context, file);
            String line = "";
            while ((line = reader.readLine()) != null) {
                String[] str = line.split(";");
                StringBuilder sb = new StringBuilder(TablasHelper.INSERT);
                sb.append(nomTabla);
                sb.append(TablasHelper.RIGHT_PARENTESIS);
                sb.append(ConstanteStandar.COL_ID);
                sb.append(TablasHelper.COMMA);
                sb.append(ConstanteStandar.COL_NOMBRE);

                if (nomTabla.equals(ConstanteStandar.TABLE_SEDE)) {
                    sb.append(TablasHelper.COMMA);
                    sb.append(ConstanteStandar.COL_ID_ESTABLECIMIENTO);
                }

                sb.append(TablasHelper.LEFT_PARENTESIS);
                sb.append(TablasHelper.VALUE);
                sb.append(TablasHelper.RIGHT_PARENTESIS);
                sb.append(str[0]);
                sb.append(TablasHelper.COMMA);
                sb.append(addComilla(str[1]));

                if (nomTabla.equals(ConstanteStandar.TABLE_SEDE)) {
                    sb.append(TablasHelper.COMMA);
                    sb.append(str[2]);
                }

                sb.append(TablasHelper.LEFT_PARENTESIS);
                db.execSQL(sb.toString());
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Metodo encargado de leer el archivo cvs del estudiante y almacena en l aBD
     */
    private void regEstudiante() {

        if (!validaDatos(ConstantesEstudiante.TABLE_NAME)) {
            return;
        }
        try {
            BufferedReader reader = Utils.getDatosFile(context, FILE_ESTUDIANTE);
            String line = "";
            while ((line = reader.readLine()) != null) {
                String[] str = line.split(";");
                StringBuilder sb = new StringBuilder(TablasHelper.INSERT);
                sb.append(ConstantesEstudiante.TABLE_NAME);
                sb.append(TablasHelper.RIGHT_PARENTESIS);
                sb.append(ConstantesEstudiante.COL_ID);
                sb.append(TablasHelper.COMMA);
                sb.append(ConstantesEstudiante.COL_NOM_1);
                sb.append(TablasHelper.COMMA);
                sb.append(ConstantesEstudiante.COL_NOM_2);
                sb.append(TablasHelper.COMMA);
                sb.append(ConstantesEstudiante.COL_APE_1);
                sb.append(TablasHelper.COMMA);
                sb.append(ConstantesEstudiante.COL_APE_2);
                sb.append(TablasHelper.COMMA);
                sb.append(ConstantesEstudiante.COL_NAC);
                sb.append(TablasHelper.COMMA);
                sb.append(ConstantesEstudiante.COL_CEDULA);
                sb.append(TablasHelper.COMMA);
                sb.append(ConstantesEstudiante.COL_ESTABLECIMIENTO);
                sb.append(TablasHelper.COMMA);
                sb.append(ConstantesEstudiante.COL_SEDE);
                sb.append(TablasHelper.COMMA);
                sb.append(ConstantesEstudiante.COL_JORNADA);
                sb.append(TablasHelper.COMMA);
                sb.append(ConstantesEstudiante.COL_GRUPO);
                sb.append(TablasHelper.COMMA);
                sb.append(ConstantesEstudiante.COL_GRADO);
                sb.append(TablasHelper.COMMA);
                sb.append(ConstantesEstudiante.COL_PATH);
                sb.append(TablasHelper.LEFT_PARENTESIS);
                sb.append(TablasHelper.VALUE);
                sb.append(TablasHelper.RIGHT_PARENTESIS);
                sb.append(str[0]);
                sb.append(TablasHelper.COMMA);
                sb.append(addComilla(str[1]));
                sb.append(TablasHelper.COMMA);
                sb.append(addComilla(str[2]));
                sb.append(TablasHelper.COMMA);
                sb.append(addComilla(str[3]));
                sb.append(TablasHelper.COMMA);
                sb.append(addComilla(str[4]));
                sb.append(TablasHelper.COMMA);
                sb.append(addComilla(str[5]));
                sb.append(TablasHelper.COMMA);
                sb.append(addComilla(str[6]));
                sb.append(TablasHelper.COMMA);
                sb.append(str[7]);
                sb.append(TablasHelper.COMMA);
                sb.append(str[8]);
                sb.append(TablasHelper.COMMA);
                sb.append(str[9]);
                sb.append(TablasHelper.COMMA);
                sb.append(str[10]);
                sb.append(TablasHelper.COMMA);
                sb.append(str[11]);
                sb.append(TablasHelper.COMMA);
                sb.append(str[12]);
                sb.append(TablasHelper.LEFT_PARENTESIS);
                db.execSQL(sb.toString());
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Metodo encargao de validar datos
     * @param string
     * @return
     */
    private Boolean validaDatos(String string) {
        Cursor cursor = TablasHelper.getDatos(db, string);
        if (cursor.getCount() > 0) {
            return false;
        }
        return true;
    }


}
