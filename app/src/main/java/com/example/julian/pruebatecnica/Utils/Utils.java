package com.example.julian.pruebatecnica.Utils;

import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.provider.MediaStore;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

public class Utils {

    public static String addComilla(String string) {
        return "'" + string + "'";
    }

    public static BufferedReader getDatosFile(Context context, String file) throws IOException {
        InputStream is = context.getAssets().open(file);
        return new BufferedReader(new InputStreamReader(is));
    }


    /**
     * Metodo para obtener la uri de una imagen
     * @param context
     * @param inImage
     * @return
     */
    public static Uri getImagenUri(Context context, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(context.getContentResolver(), inImage, "Title", null);
        return Uri.parse(path);
    }

    /**
     * Metodo para obtener el path donde se ubica la imagen
     * @param context
     * @param uri
     * @return
     */
    public static String getRealPathURI(Context context,Uri uri) {
        Cursor cursor = context.getContentResolver().query(uri, null, null, null, null);
        cursor.moveToFirst();
        int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
        return cursor.getString(idx);
    }
}
