package com.example.julian.pruebatecnica.Servicio;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.example.julian.pruebatecnica.Aplicacion;
import com.example.julian.pruebatecnica.BD.Constantes.ConstantesUsuario;
import com.example.julian.pruebatecnica.BD.SqlLiteHelper;
import com.example.julian.pruebatecnica.BD.TablasHelper;
import com.example.julian.pruebatecnica.Modelo.Usuario;

import javax.inject.Inject;

public class ServicioUsuario {

    @Inject
    SqlLiteHelper sqlLiteHelper;

    SQLiteDatabase db;

    public ServicioUsuario() {
        Aplicacion.getApp().getComponente().inject(this);
        db = sqlLiteHelper.getReadableDatabase();
    }

    public Boolean getSesion(Usuario usuario) {
        Cursor usuarios = TablasHelper.getDatos(db,ConstantesUsuario.TABLE_NAME);
        if (usuarios.moveToFirst()) {
            do {
                if (usuario.getUsuario().equals(usuarios.getString(1))
                        && usuario.getContrasena().equals(usuarios.getString(2))) {
                    return true;
                }
            } while (usuarios.moveToNext());
        }

        return false;
    }
}
