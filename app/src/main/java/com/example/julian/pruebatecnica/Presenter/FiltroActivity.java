package com.example.julian.pruebatecnica.Presenter;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.julian.pruebatecnica.Aplicacion;
import com.example.julian.pruebatecnica.BD.Constantes.ConstanteStandar;
import com.example.julian.pruebatecnica.Modelo.Estudiante;
import com.example.julian.pruebatecnica.Modelo.ModeloStandar;
import com.example.julian.pruebatecnica.R;
import com.example.julian.pruebatecnica.Servicio.ServicioEstudiante;
import com.example.julian.pruebatecnica.Servicio.ServicioInformacionStandar;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

public class FiltroActivity extends AppCompatActivity implements View.OnClickListener{

    /*Variable de tipo EditText en donde se recibe el establecimiento ingresado*/
    @BindView(R.id.sp_establecimiento)
    Spinner spEstablecimiento;
    /*Variable de tipo EditText en donde se recibe el sede ingresado*/
    @BindView(R.id.sp_sede)
    Spinner spSede;
    /*Variable de tipo EditText en donde se recibe el jornada ingresado*/
    @BindView(R.id.sp_jornada)
    Spinner spJornada;
    /*Variable de tipo EditText en donde se recibe el grupo ingresado*/
    @BindView(R.id.sp_grupo)
    Spinner spGrupo;
    /*Variable de tipo EditText en donde se recibe el grada ingresado*/
    @BindView(R.id.sp_grado)
    Spinner spGrado;

    @BindView(R.id.lv_listview)
    ListView listView;

    /*Variable de tipo Button en donde se ejecuta el método OnClick*/
    @BindView(R.id.ingresar_btn)
    Button btnConsulta;

    /*Injeccion de dependencias*/
    @Inject
    ServicioInformacionStandar servicioInformacionStandar;
    @Inject
    ServicioEstudiante servicioEstudiante;
    @Inject
    ModeloStandar establecimiento;
    @Inject
    ModeloStandar jornada;
    @Inject
    ModeloStandar grupo;
    @Inject
    ModeloStandar grado;
    @Inject
    ModeloStandar establecimientoFiltro;
    @Inject
    ModeloStandar sedeFiltro;
    @Inject
    ModeloStandar jornadaFiltro;
    @Inject
    ModeloStandar grupoFiltro;
    @Inject
    ModeloStandar gradoFiltro;
    @Inject
    Estudiante estudiante ;

    /**Definicion de variab**/
    List<ModeloStandar> listEstablecimiento;
    List<ModeloStandar> listJornada;
    List<ModeloStandar> listGrupo;
    List<ModeloStandar> listGrado;
    List<Estudiante> estudianteFiltroList = new ArrayList<Estudiante>();
    List<Estudiante> estudianteRar = new ArrayList<Estudiante>();

    Adapter adapter;

    Integer valEstablecimiento = null;
    Integer valSede = null;
    Integer valJornada = null;
    Integer valGrupo = null;
    Integer valGrado = null;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_filtro);
        ButterKnife.bind(this);
        ((Aplicacion) getApplication()).getComponente().inject(this);
        inicializaSpinner();
        inicializaEstudiantes();
        spEstablecimiento.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (spEstablecimiento.getSelectedItemPosition() != 0) {
                    inicializaSede((ModeloStandar) spEstablecimiento.getSelectedItem());
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Estudiante estu = (Estudiante) listView.getAdapter().getItem(position);
                Intent intent = new Intent(FiltroActivity.this, EditaEstudianteActivity.class);
                intent.putExtra("Estudiante", estu);
                startActivity(intent);
                finish();
            }
        });
    }

    /**
     * metodo para inicializar los spinneres del metodo estandar
     */
    private void inicializaSpinner() {

        establecimiento.setNombre(getString(R.string.establecimiento));
        listEstablecimiento = servicioInformacionStandar.getInformacionStandar(ConstanteStandar.TABLE_ESTABLECIMIENTO);
        listEstablecimiento.add(0,establecimiento);
        ArrayAdapter establecimiento = new ArrayAdapter(this, R.layout.spinner, listEstablecimiento);
        spEstablecimiento.setAdapter(establecimiento);

        jornada.setNombre(getString(R.string.jornada));
        listJornada = servicioInformacionStandar.getInformacionStandar(ConstanteStandar.TABLE_JORNADA);
        listJornada.add(0,jornada);
        ArrayAdapter jornada = new ArrayAdapter(this, R.layout.spinner, listJornada);
        spJornada.setAdapter(jornada);

        grupo.setNombre(getString(R.string.grupo));
        listGrupo = servicioInformacionStandar.getInformacionStandar(ConstanteStandar.TABLE_GRUPO);
        listGrupo.add(0,grupo);
        ArrayAdapter grupo = new ArrayAdapter(this, R.layout.spinner, listGrupo);
        spGrupo.setAdapter(grupo);

        grado.setNombre(getString(R.string.grado));
        listGrado = servicioInformacionStandar.getInformacionStandar(ConstanteStandar.TABLE_GRADO);
        listGrado.add(0,grado);
        ArrayAdapter grado = new ArrayAdapter(this, R.layout.spinner, listGrado);
        spGrado.setAdapter(grado);
    }

    /**
     * Meotodo para inicialiar los spinners de las sedes
     * @param modeloStandar
     */
    private void inicializaSede(ModeloStandar modeloStandar){
        ModeloStandar sedeModelo = new ModeloStandar();
        sedeModelo.setNombre(getString(R.string.sede));
        List<ModeloStandar> listSede = servicioInformacionStandar.getInformacionSede(modeloStandar.getId());
        listSede.add(0,sedeModelo);
        ArrayAdapter sede = new ArrayAdapter(this, R.layout.spinner, listSede);
        spSede.setAdapter(sede);
    }

    /**
     * Metodo para inicializar las lista de los estudiantes
     */
    private void inicializaEstudiantes(){
        List<Estudiante> estudianteList = servicioEstudiante.getEstudiante();
        adapter = new Adapter(this,estudianteList);
        listView.setAdapter(adapter);
    }
 /** metodo que ejecuta el evento onclick**/
    @Override
    public void onClick(View v) {
        ejecutarConsulta();
    }

    /**
     * metodo encargado de ejecutar el boton de consulta de los filtros de busqueda
     */
    private void ejecutarConsulta() {
        estudianteRar.clear();
        estudianteFiltroList = servicioEstudiante.getEstudianteFiltro();
        int contador = 0;

        if (spEstablecimiento.getSelectedItemPosition() != 0 &&
                spSede.getSelectedItemPosition() != 0 &&
                spJornada.getSelectedItemPosition() != 0 &&
                spGrupo.getSelectedItemPosition() != 0 &&
                spGrado.getSelectedItemPosition() != 0 ) {

            for (Estudiante est : estudianteFiltroList) {
                if(validacionEstudianteFiltro(est)){
                    estudianteRar.add(est);
                    contador += 1;
                }

            }
            if(contador == 0){
                estudianteRar.clear();
                Toast.makeText(getApplicationContext(), R.string.sin_datos, Toast.LENGTH_SHORT).show();
            }
            adapter = new Adapter(this,estudianteRar);
            listView.setAdapter(adapter);
        }else{
            Toast.makeText(getApplicationContext(), R.string.error_filtro, Toast.LENGTH_SHORT).show();
            estudianteRar = servicioEstudiante.getEstudianteFiltro();
            adapter = new Adapter(this,estudianteRar);
            listView.setAdapter(adapter);
            inicializaSpinner();
            spSede.setAdapter(null);
        }
    }

    /**
     * Metodo encargado de validar si la informacion de los filtros coincide con la obtenida en los estudiantes
     * @param estudianteFiltro
     * @return
     */
    private boolean validacionEstudianteFiltro(Estudiante estudianteFiltro) {

        Estudiante objeto = getObjetoEstudiante();
        valEstablecimiento = estudianteFiltro.getIdEstablecimiento();
        valSede = estudianteFiltro.getIdSede();
        valJornada = estudianteFiltro.getIdJornada();
        valGrupo = estudianteFiltro.getIdGrupo();
        valGrado = estudianteFiltro.getIdGrado();

        if(!objeto.getIdEstablecimiento().equals(valEstablecimiento)){
            return false;
        }

        if(!objeto.getIdSede().equals(valSede)){
            return false;
        }

        if(!objeto.getIdJornada().equals(valJornada)){
            return false;
        }

        if(!objeto.getIdGrupo().equals(valGrupo)){
            return false;
        }

        if(!objeto.getIdGrado().equals(valGrado)){
            return false;
        }

        return true;
    }

    /**
     *
     * Metodo encargado de llenar el objeto estudiante con la informacion obtenida de los filtros
     * @return Estudiante
     */
    private Estudiante getObjetoEstudiante() {

        Estudiante estudiantes = new Estudiante();
        establecimientoFiltro = (ModeloStandar) spEstablecimiento.getSelectedItem();
        sedeFiltro = (ModeloStandar) spSede.getSelectedItem();
        jornadaFiltro = (ModeloStandar) spJornada.getSelectedItem();
        grupoFiltro = (ModeloStandar) spGrupo.getSelectedItem();
        gradoFiltro = (ModeloStandar) spGrado.getSelectedItem();

        estudiantes.setIdEstablecimiento(establecimientoFiltro.getId());
        estudiantes.setIdSede(sedeFiltro.getId());
        estudiantes.setIdJornada(jornadaFiltro.getId());
        estudiantes.setIdGrupo(grupoFiltro.getId());
        estudiantes.setIdGrado(gradoFiltro.getId());

        return estudiantes;
    }
}
