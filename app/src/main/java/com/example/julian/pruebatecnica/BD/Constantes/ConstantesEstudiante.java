package com.example.julian.pruebatecnica.BD.Constantes;

public final class ConstantesEstudiante {

    /*Nombre de la tabla Estudiante*/
    public static final String TABLE_NAME = "estudiante";

    /*Nombre Columna Tabla Estudiante*/
    public static final String COL_ID = "id";
    public static final String COL_NOM_1 = "primerNombre";
    public static final String COL_NOM_2 = "segungoNombre";
    public static final String COL_APE_1 = "primerApellido";
    public static final String COL_APE_2 = "segungoApellido";
    public static final String COL_NAC = "fechaNacimiento";
    public static final String COL_CEDULA = "Identificacion";
    public static final String COL_ESTABLECIMIENTO = "idEstablecimiento";
    public static final String COL_SEDE = "idSede";
    public static final String COL_JORNADA = "idJornada";
    public static final String COL_GRUPO = "idGrupo";
    public static final String COL_GRADO = "idGrado";
    public static final String COL_PATH = "rutaImagen";
}
