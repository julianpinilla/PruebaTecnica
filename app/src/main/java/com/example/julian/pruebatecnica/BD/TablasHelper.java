package com.example.julian.pruebatecnica.BD;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.example.julian.pruebatecnica.BD.Constantes.ConstanteStandar;
import com.example.julian.pruebatecnica.BD.Constantes.ConstantesEstudiante;
import com.example.julian.pruebatecnica.BD.Constantes.ConstantesUsuario;

/**
 * Clase que crea las tablas de la base de datos local
 * @author Julian Pinilla
 */
public class TablasHelper {

    /**
     * Definicion de constantes
     */
    protected static final String CREATE = "CREATE TABLE ";
    protected static final String RIGHT_PARENTESIS = " ( ";
    protected static final String LEFT_PARENTESIS = " ) ";
    protected static final String COMMA = ",";
    protected static final String INSERT = "INSERT INTO ";
    protected static final String VALUE = " VALUES ";
    protected static final String ATERISCO = "*";

    /**
     * Constructor
     */
    public static void crearTablas(SQLiteDatabase db) {
        tablaUsuario(db);
        tablaStandar(db,ConstanteStandar.TABLE_ESTABLECIMIENTO);
        tablaStandar(db,ConstanteStandar.TABLE_SEDE);
        tablaStandar(db,ConstanteStandar.TABLE_JORNADA);
        tablaStandar(db,ConstanteStandar.TABLE_GRUPO);
        tablaStandar(db,ConstanteStandar.TABLE_GRADO);
        tablaEstudiante(db);
    }

    /**
     * Metodo que creala tabla Usuario
     */
    private static void tablaUsuario(SQLiteDatabase db){
        Log.e("info", "si entro");
        StringBuilder sql = new StringBuilder(CREATE);
        sql.append(ConstantesUsuario.TABLE_NAME);
        sql.append(RIGHT_PARENTESIS);
        sql.append(ConstantesUsuario.COL_ID);
        sql.append(COMMA);
        sql.append(ConstantesUsuario.COL_USER);
        sql.append(COMMA);
        sql.append(ConstantesUsuario.COL_PASS);
        sql.append(LEFT_PARENTESIS);

        Log.e("tabla", sql.toString());

        db.execSQL(sql.toString());
    }

    /**
     * Metodo que crea las tablas Standar
     */
    private static void tablaStandar(SQLiteDatabase db, String nomTabla){
        Log.e("info", "si entro");
        StringBuilder sql = new StringBuilder(CREATE);
        sql.append(nomTabla);
        sql.append(RIGHT_PARENTESIS);
        sql.append(ConstanteStandar.COL_ID);
        sql.append(COMMA);
        sql.append(ConstanteStandar.COL_NOMBRE);
        if (nomTabla.equals(ConstanteStandar.TABLE_SEDE)) {
            sql.append(TablasHelper.COMMA);
            sql.append(ConstanteStandar.COL_ID_ESTABLECIMIENTO);
        }
        sql.append(LEFT_PARENTESIS);

        Log.e("tabla", sql.toString());

        db.execSQL(sql.toString());
    }

    /**
     * Metodo que creala tabla Usuario
     */
    private static void tablaEstudiante(SQLiteDatabase db){
        Log.e("info", "si entro");
        StringBuilder sql = new StringBuilder(CREATE);
        sql.append(ConstantesEstudiante.TABLE_NAME);
        sql.append(RIGHT_PARENTESIS);
        sql.append(ConstantesEstudiante.COL_ID);
        sql.append(COMMA);
        sql.append(ConstantesEstudiante.COL_NOM_1);
        sql.append(COMMA);
        sql.append(ConstantesEstudiante.COL_NOM_2);
        sql.append(COMMA);
        sql.append(ConstantesEstudiante.COL_APE_1);
        sql.append(COMMA);
        sql.append(ConstantesEstudiante.COL_APE_2);
        sql.append(COMMA);
        sql.append(ConstantesEstudiante.COL_NAC);
        sql.append(COMMA);
        sql.append(ConstantesEstudiante.COL_CEDULA);
        sql.append(COMMA);
        sql.append(ConstantesEstudiante.COL_ESTABLECIMIENTO);
        sql.append(COMMA);
        sql.append(ConstantesEstudiante.COL_SEDE);
        sql.append(COMMA);
        sql.append(ConstantesEstudiante.COL_JORNADA);
        sql.append(COMMA);
        sql.append(ConstantesEstudiante.COL_GRUPO);
        sql.append(COMMA);
        sql.append(ConstantesEstudiante.COL_GRADO);
        sql.append(COMMA);
        sql.append(ConstantesEstudiante.COL_PATH);
        sql.append(LEFT_PARENTESIS);

        Log.e("tabla", sql.toString());

        db.execSQL(sql.toString());
    }

    public static Cursor getDatos(SQLiteDatabase db, String string){
        String[] campos = new String[]{ATERISCO};
        Cursor cursor = db.query(string, campos, null, null, null, null, null);
        return cursor;
    }
}
