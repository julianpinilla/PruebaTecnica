package com.example.julian.pruebatecnica.Modelo;

import java.io.Serializable;

public class ModeloStandar implements Serializable{

    private Integer id;
    private String nombre;
    private Integer idEstablecimiento;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Integer getIdEstablecimiento() {
        return idEstablecimiento;
    }

    public void setIdEstablecimiento(Integer idEstablecimiento) {
        this.idEstablecimiento = idEstablecimiento;
    }

    @Override
    public String toString() {
        return this.nombre;
    }
}
