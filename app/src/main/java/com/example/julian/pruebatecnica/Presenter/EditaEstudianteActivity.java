package com.example.julian.pruebatecnica.Presenter;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.julian.pruebatecnica.Aplicacion;
import com.example.julian.pruebatecnica.Modelo.Estudiante;
import com.example.julian.pruebatecnica.R;
import com.example.julian.pruebatecnica.Servicio.ServicioEstudiante;
import com.example.julian.pruebatecnica.Utils.Utils;

import java.io.File;
import java.util.Calendar;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

public class EditaEstudianteActivity extends AppCompatActivity {

    /*Variable de tipo EditText en donde se recibe el primer nombre ingresado*/
    @BindView(R.id.et_nombre1)
    EditText nombre1;
    /*Variable de tipo EditText en donde se recibe el segundo nombre ingresado*/
    @BindView(R.id.et_nombre2)
    EditText nombre2;
    /*Variable de tipo EditText en donde se recibe el primer apellido ingresado*/
    @BindView(R.id.et_ape1)
    EditText apellido1;
    /*Variable de tipo EditText en donde se recibe el segundo apellido ingresado*/
    @BindView(R.id.et_ape2)
    EditText apellido2;
    /*Variable de tipo EditText en donde se recibe la identificacion ingresado*/
    @BindView(R.id.et_cedula)
    EditText cedula;
    /*Variable de tipo EditText en donde se recibe la fecha de nacimiento ingresado*/
    @BindView(R.id.et_fecha)
    TextView fecha;
    /*Variable de tipo Button en donde se ejecuta el método OnClick*/
    @BindView(R.id.ingresar_btn)
    Button btnlogin;
    @BindView(R.id.iv_foto)
    ImageView mfoto;

    /*Inyeccion de dependencias*/
    @Inject
    Estudiante estudiante ;
    @Inject
    Estudiante estudianteModificado ;
    @Inject
    ServicioEstudiante servicioEstudiante;

    private static final Integer REQUEST_CAMERA = 1;
    private static final Integer SELECT_FILE = 0;
    private static final CharSequence[] ARRAY_IMAGE = {"Camara", "Galeria", "Cancelar"};
    private String path = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edita_estudiante);
        ButterKnife.bind(this);
        ((Aplicacion) getApplication()).getComponente().inject(this);

        Intent intent = getIntent();
        estudiante = (Estudiante) intent.getSerializableExtra("Estudiante");
        nombre1.setText(estudiante.getPrimerNombre());
        nombre2.setText(estudiante.getSegungoNombre());
        apellido1.setText(estudiante.getPrimerApellido());
        apellido2.setText(estudiante.getSegungoApellido());
        cedula.setText(estudiante.getIdentificacion());
        fecha.setText(estudiante.getFechaNacimiento());
        if (estudiante.getRutaImagen() != null) {
            mfoto.setImageURI(Uri.fromFile(new File(estudiante.getRutaImagen())));
        }
        mfoto.setOnClickListener(imagenes);
        btnlogin.setOnClickListener(editar);
    }
    /**
     * Metodo que valida los campos que seran editados
     * @return
     */
    private boolean validaCampos() {
        boolean retorna = true;
        if(nombre1.getText().equals(null) || nombre1.getText().toString().trim().equals("")){
            retorna = false;
        }
        if(nombre2.getText().equals(null) || nombre2.getText().toString().trim().equals("")){
            retorna = false;
        }
        if(apellido1.getText().equals(null) || apellido2.getText().toString().trim().equals("")){
            retorna = false;
        }
        if(apellido2.getText().equals(null) || apellido2.getText().toString().trim().equals("")){
            retorna = false;
        }
        if(cedula.getText().equals(null) || cedula.getText().toString().trim().equals("")){
            retorna = false;
        }
        if(fecha.getText().equals(null) || fecha.getText().toString().trim().equals("")){
            retorna = false;
        }

        return retorna;
    }

    /**
     * Este metodo llena el objeto estudiante y lo envia al servicio para que este ejecute el update
     */
    private void editaDatos() {
        estudianteModificado.setPrimerNombre(nombre1.getText().toString());
        estudianteModificado.setSegungoNombre(nombre2.getText().toString());
        estudianteModificado.setPrimerApellido(apellido1.getText().toString());
        estudianteModificado.setSegungoApellido(apellido2.getText().toString());
        estudianteModificado.setIdentificacion(cedula.getText().toString());
        estudianteModificado.setFechaNacimiento(fecha.getText().toString());
        estudianteModificado.setId(estudiante.getId());
        if (path != null) {
            estudianteModificado.setRutaImagen(path);
        }
        int valor = servicioEstudiante.getEstudianteEdita(estudianteModificado);

        if (valor ==1){
        Toast.makeText(getApplicationContext(), getResources().getString(R.string.editar_exito), Toast.LENGTH_SHORT).show();
        Intent intent = new Intent(this, FiltroActivity.class);
        startActivity(intent);
        }

    }


    View.OnClickListener imagenes = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            obtenerImagen();
        }
    };

    View.OnClickListener editar = new View.OnClickListener() {
            @Override
        public void onClick(View v) {
            if(validaCampos()){
                editaDatos();
            }
        }
    };

    /**
     * Este metodo inicializa el intent de camptura fotografica
     */
    private void obtenerImagen() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(getString(R.string.seleccione));
        builder.setItems(ARRAY_IMAGE, new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                if (ARRAY_IMAGE[i].equals(getString(R.string.camara))) {
                    Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    startActivityForResult(intent, REQUEST_CAMERA);
                } else if (ARRAY_IMAGE[i].equals(getString(R.string.galeria))) {
                    Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    intent.setType("image/*");
                    startActivityForResult(intent, SELECT_FILE);
                } else if (ARRAY_IMAGE[i].equals(getString(R.string.cancelar))) {
                    dialogInterface.dismiss();
                }
            }
        });
        builder.show();

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Uri selectedImageUri = null;
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == REQUEST_CAMERA) {
                Bundle bundle = data.getExtras();
                final Bitmap bmp = (Bitmap) bundle.get("data");
                selectedImageUri = Utils.getImagenUri(EditaEstudianteActivity.this, bmp);
            } else if (requestCode == SELECT_FILE) {
                selectedImageUri = data.getData();
            }
            mfoto.setImageURI(selectedImageUri);
            path = Utils.getRealPathURI(EditaEstudianteActivity.this, selectedImageUri);
            Log.e("path", path);
        }
    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent = new Intent(EditaEstudianteActivity.this, FiltroActivity.class);
        startActivity(intent);
        finish();

    }

}
