package com.example.julian.pruebatecnica.Di;

import com.example.julian.pruebatecnica.BD.RegDatosBd;
import com.example.julian.pruebatecnica.BD.SqlLiteHelper;
import com.example.julian.pruebatecnica.MainActivity;
import com.example.julian.pruebatecnica.Presenter.FiltroActivity;
import com.example.julian.pruebatecnica.Presenter.EditaEstudianteActivity;
import com.example.julian.pruebatecnica.Servicio.ServicioEstudiante;
import com.example.julian.pruebatecnica.Servicio.ServicioInformacionStandar;
import com.example.julian.pruebatecnica.Servicio.ServicioUsuario;

import dagger.Component;

@Component(modules = {Modulo.class})
public interface Componente {
    void inject(SqlLiteHelper sqlLiteHelper);
    void inject(MainActivity mainActivity);
    void inject(RegDatosBd regDatosBd);
    void inject(ServicioUsuario servicioUsuario);
    void inject(ServicioInformacionStandar servicioInformacionStandar);
    void inject(FiltroActivity filtroActivity);
    void inject (ServicioEstudiante servicioEstudiante);
    void inject(EditaEstudianteActivity editaEstudianteActivity);
}
