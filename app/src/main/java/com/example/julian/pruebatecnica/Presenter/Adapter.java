package com.example.julian.pruebatecnica.Presenter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.julian.pruebatecnica.Modelo.Estudiante;
import com.example.julian.pruebatecnica.R;

import java.util.List;

public class Adapter extends BaseAdapter {


    private Context mContext;
    protected List<Estudiante> listEstudiante;

    public Adapter(Context context, List<Estudiante> estudiante) {
        this.mContext = context;
        this.listEstudiante = estudiante;
    }

    @Override
    public int getCount() {
        return listEstudiante.size();
    }

    @Override
    public Object getItem(int i) {
        return listEstudiante.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        Log.e("cantidad", String.valueOf(i));
        final ViewHolder holder;
        View v = view;
        if (v == null) {
            LayoutInflater inf = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = inf.inflate(R.layout.adapter, null);
            holder = new ViewHolder();
            holder.nombre1 = (TextView) v.findViewById(R.id.nombre1);
            holder.nombre2 = (TextView) v.findViewById(R.id.nombre2);
            holder.apellido1 = (TextView) v.findViewById(R.id.apellido1);
            holder.apellido2 = (TextView) v.findViewById(R.id.apellido2);
            holder.fecha = (TextView) v.findViewById(R.id.fecha_nacimiento);
            holder.cedula = (TextView) v.findViewById(R.id.cedula);
            v.setTag(holder);
        } else {
            holder = (ViewHolder) v.getTag();
        }

        Estudiante dir = listEstudiante.get(i);
        holder.nombre1.setText(dir.getPrimerNombre());
        holder.nombre2.setText(dir.getSegungoNombre());
        holder.apellido1.setText(dir.getPrimerApellido());
        holder.apellido2.setText(dir.getSegungoApellido());
        holder.fecha.setText(dir.getFechaNacimiento());
        holder.cedula.setText(dir.getIdentificacion());

        return v;
    }

    private static class ViewHolder {
        TextView nombre1;
        TextView nombre2;
        TextView apellido1;
        TextView apellido2;
        TextView fecha;
        TextView cedula;
    }
}
