package com.example.julian.pruebatecnica.Modelo;

import java.io.Serializable;

/**
 * Clase encargada de definir el modelo de la tabla estudiantes
 */
public class Estudiante implements Serializable{

    public   Integer   id;
    public   String   primerNombre;
    public   String   segungoNombre;
    public   String   primerApellido;
    public   String   segungoApellido;
    public   String   fechaNacimiento;
    public   String  Identificacion;
    public   Integer   idEstablecimiento;
    public   Integer   idSede;
    public   Integer   idJornada;
    public   Integer   idGrupo;
    public   Integer   idGrado;
    public   String   rutaImagen;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getPrimerNombre() {
        return primerNombre;
    }

    public void setPrimerNombre(String primerNombre) {
        this.primerNombre = primerNombre;
    }

    public String getSegungoNombre() {
        return segungoNombre;
    }

    public void setSegungoNombre(String segungoNombre) {
        this.segungoNombre = segungoNombre;
    }

    public String getPrimerApellido() {
        return primerApellido;
    }

    public void setPrimerApellido(String primerApellido) {
        this.primerApellido = primerApellido;
    }

    public String getSegungoApellido() {
        return segungoApellido;
    }

    public void setSegungoApellido(String segungoApellido) {
        this.segungoApellido = segungoApellido;
    }

    public String getFechaNacimiento() {
        return fechaNacimiento;
    }

    public void setFechaNacimiento(String fechaNacimiento) {
        this.fechaNacimiento = fechaNacimiento;
    }

    public String getIdentificacion() {
        return Identificacion;
    }

    public void setIdentificacion(String identificacion) {
        Identificacion = identificacion;
    }

    public Integer getIdEstablecimiento() {
        return idEstablecimiento;
    }

    public void setIdEstablecimiento(Integer idEstablecimiento) {
        this.idEstablecimiento = idEstablecimiento;
    }

    public Integer getIdSede() {
        return idSede;
    }

    public void setIdSede(Integer idSede) {
        this.idSede = idSede;
    }

    public Integer getIdJornada() {
        return idJornada;
    }

    public void setIdJornada(Integer idJornada) {
        this.idJornada = idJornada;
    }

    public Integer getIdGrupo() {
        return idGrupo;
    }

    public void setIdGrupo(Integer idGrupo) {
        this.idGrupo = idGrupo;
    }

    public Integer getIdGrado() {
        return idGrado;
    }

    public void setIdGrado(Integer idGrado) {
        this.idGrado = idGrado;
    }

    public String getRutaImagen() {
        return rutaImagen;
    }

    public void setRutaImagen(String rutaImagen) {
        this.rutaImagen = rutaImagen;
    }
}
