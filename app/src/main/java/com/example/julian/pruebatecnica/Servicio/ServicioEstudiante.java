package com.example.julian.pruebatecnica.Servicio;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.example.julian.pruebatecnica.Aplicacion;
import com.example.julian.pruebatecnica.BD.Constantes.ConstantesEstudiante;
import com.example.julian.pruebatecnica.BD.SqlLiteHelper;
import com.example.julian.pruebatecnica.BD.TablasHelper;
import com.example.julian.pruebatecnica.Modelo.Estudiante;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

public class ServicioEstudiante {

    @Inject
    SqlLiteHelper sqlLiteHelper;
    SQLiteDatabase db;

    /**
     *
     */
    public ServicioEstudiante() {
        Aplicacion.getApp().getComponente().inject(this);
        db = sqlLiteHelper.getReadableDatabase();
    }

    /**
     * metodo encargado de traer la informacin basica de los estudiantes
     * @return
     */
    public List<Estudiante> getEstudiante() {
        Cursor cEstudiante = TablasHelper.getDatos(db, ConstantesEstudiante.TABLE_NAME);
        List<Estudiante> estudianteList = new ArrayList<Estudiante>();
        if (cEstudiante.moveToFirst()) {
            for(cEstudiante.moveToFirst(); !cEstudiante.isAfterLast(); cEstudiante.moveToNext()) {
                    Estudiante estudiante = new Estudiante();
                    estudiante.setId(cEstudiante.getInt(0));
                    estudiante.setPrimerNombre(cEstudiante.getString(1));
                    estudiante.setSegungoNombre(cEstudiante.getString(2));
                    estudiante.setPrimerApellido(cEstudiante.getString(3));
                    estudiante.setSegungoApellido(cEstudiante.getString(4));
                    estudiante.setFechaNacimiento(cEstudiante.getString(5));
                    estudiante.setIdentificacion(cEstudiante.getString(6));
                estudianteList.add(estudiante);
            }
        }

        return estudianteList;
    }

    /**
     * metodo que trae la consulta los estudiantes con todos los registros
     * @return
     */
    public List<Estudiante> getEstudianteFiltro() {
        Cursor cEstudiante = TablasHelper.getDatos(db, ConstantesEstudiante.TABLE_NAME);
        List<Estudiante> estudianteList = new ArrayList<Estudiante>();
        if (cEstudiante.moveToFirst()) {
            for(cEstudiante.moveToFirst(); !cEstudiante.isAfterLast(); cEstudiante.moveToNext()) {
                Estudiante estudiante = new Estudiante();
                estudiante.setId(cEstudiante.getInt(0));
                estudiante.setPrimerNombre(cEstudiante.getString(1));
                estudiante.setSegungoNombre(cEstudiante.getString(2));
                estudiante.setPrimerApellido(cEstudiante.getString(3));
                estudiante.setSegungoApellido(cEstudiante.getString(4));
                estudiante.setFechaNacimiento(cEstudiante.getString(5));
                estudiante.setIdentificacion(cEstudiante.getString(6));
                estudiante.setIdEstablecimiento(cEstudiante.getInt(7));
                estudiante.setIdSede(cEstudiante.getInt(8));
                estudiante.setIdJornada(cEstudiante.getInt(9));
                estudiante.setIdGrupo(cEstudiante.getInt(10));
                estudiante.setIdGrado(cEstudiante.getInt(11));
                estudianteList.add(estudiante);
            }
        }

        return estudianteList;
    }

    /**
     * Metodo encargado de realizar la edicion de los datos
     * @param estudiante
     * @return
     */
    public int getEstudianteEdita(Estudiante estudiante) {
        int cod =estudiante.getId();
        ContentValues valores = new ContentValues();
        valores.put(ConstantesEstudiante.COL_NOM_1, estudiante.getPrimerNombre());
        valores.put(ConstantesEstudiante.COL_NOM_2, estudiante.getSegungoApellido());
        valores.put(ConstantesEstudiante.COL_APE_1, estudiante.getPrimerApellido());
        valores.put(ConstantesEstudiante.COL_APE_2, estudiante.getSegungoApellido());
        valores.put(ConstantesEstudiante.COL_CEDULA, estudiante.getIdentificacion());
        valores.put(ConstantesEstudiante.COL_NAC, estudiante.getFechaNacimiento());
        valores.put(ConstantesEstudiante.COL_PATH, estudiante.getRutaImagen());
        return db.update(ConstantesEstudiante.TABLE_NAME, valores, "id=" + cod, null);
    }
}
