package com.example.julian.pruebatecnica.BD.Constantes;

public final class ConstanteStandar {

    /*Nombre de la tabla Usuario*/
    public static final String TABLE_ESTABLECIMIENTO = "establecimiento";
    public static final String TABLE_SEDE = "sede";
    public static final String TABLE_JORNADA = "jornada";
    public static final String TABLE_GRUPO = "grupo";
    public static final String TABLE_GRADO = "grado";

    /*Nombre Columna Tabla Usuario*/
    public static final String COL_ID = "id";
    public static final String COL_NOMBRE = "nombre";
    public static final String COL_ID_ESTABLECIMIENTO = "id_establecimiento";

}
