package com.example.julian.pruebatecnica;

import android.Manifest;
import android.annotation.TargetApi;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.julian.pruebatecnica.BD.RegDatosBd;
import com.example.julian.pruebatecnica.Modelo.Usuario;
import com.example.julian.pruebatecnica.Presenter.FiltroActivity;
import com.example.julian.pruebatecnica.Servicio.ServicioUsuario;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    /*Variable de tipo EditText en donde se recibe el usuario ingresado*/
    @BindView(R.id.username_et)
    EditText user;
    /*Variable de tipo EditText en donde se recibe el usuario ingresado*/
    @BindView(R.id.contrasena_et)
    EditText password;
    /*Variable de tipo Button en donde se ejecuta el método OnClick*/
    @BindView(R.id.ingresar_btn)
    Button btnlogin;

    @Inject
    RegDatosBd regDatosBd;

    @Inject
    ServicioUsuario servicioUsuario;

    @Inject
    Usuario usuario;

    /**
     * Id de los permisos iniciales
     **/
    private static final int INITIAL_REQUEST = 1337;

    /**
     * Permisos iniciales para > Marshmallow
     **/
    private static final String[] INITIAL_PERMS = {Manifest.permission.CAMERA,
            Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        try {
            setContentView(R.layout.activity_main);
            ButterKnife.bind(this);
            ((Aplicacion) getApplication()).getComponente().inject(this);
            if (validarPermisos()) {
                regDatosBd.regInformacion();
            } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                requestPermissions(INITIAL_PERMS, INITIAL_REQUEST);
            }
            regDatosBd.regInformacion();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    /**
     * Método para validar que se haya ingresado algún usuario o contraseña,
     * luego valida esta información con la BD local y permite el inicio de sesión.
     **/

    private void ingresar() {

        String userIngreso = user.getText().toString();
        String contrasena = password.getText().toString();


        if (userIngreso.isEmpty() || contrasena.isEmpty()) {
            Toast.makeText(getApplicationContext(), R.string.datos, Toast.LENGTH_SHORT).show();
        }

        if (!userIngreso.isEmpty() && !contrasena.isEmpty()) {
            usuario.setUsuario(userIngreso);
            usuario.setContrasena(contrasena);
            if (servicioUsuario.getSesion(usuario)) {
                Toast.makeText(getApplicationContext(), getResources().getString(R.string.bienvenido) + " " + usuario.getUsuario(), Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(this, FiltroActivity.class);
                startActivity(intent);
            } else {
                Toast.makeText(getApplicationContext(), R.string.sesion_incorrecta, Toast.LENGTH_SHORT).show();
            }
        }
    }

    @Override
    public void onClick(View v) {
        ingresar();
    }

    private boolean validarPermisos() {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            return true;
        }
        for (String permission : INITIAL_PERMS) {
            if (!hasPermission(permission)) {
                return false;
            }
        }
        return true;
    }
    /**
     * Indica si tiene permisos con el parámetro dado
     *
     * @param perm Permiso a validar
     * @return True si tiene permsiso. False de lo contrario
     */
    @TargetApi(Build.VERSION_CODES.M)
    private boolean hasPermission(String perm) {
        return (PackageManager.PERMISSION_GRANTED == checkSelfPermission(perm));
    }
}
