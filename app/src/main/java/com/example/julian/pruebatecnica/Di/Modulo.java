package com.example.julian.pruebatecnica.Di;

import android.content.Context;

import com.example.julian.pruebatecnica.BD.RegDatosBd;
import com.example.julian.pruebatecnica.BD.SqlLiteHelper;
import com.example.julian.pruebatecnica.Modelo.Estudiante;
import com.example.julian.pruebatecnica.Modelo.ModeloStandar;
import com.example.julian.pruebatecnica.Modelo.Usuario;
import com.example.julian.pruebatecnica.Servicio.ServicioEstudiante;
import com.example.julian.pruebatecnica.Servicio.ServicioInformacionStandar;
import com.example.julian.pruebatecnica.Servicio.ServicioUsuario;

import dagger.Module;
import dagger.Provides;

@Module
public class Modulo {

    private final Context context;

    public Modulo(Context context) {
        this.context = context;
        new SqlLiteHelper(context);
    }

    @Provides
    public SqlLiteHelper provideSqlLiteHelper() {
        return new SqlLiteHelper(context);
    }

    @Provides
    public RegDatosBd provideRegDatosBd() {
        return new RegDatosBd(context);
    }

    @Provides
    public ServicioUsuario provideServicioUsuario() {
        return new ServicioUsuario();
    }

    @Provides
    public Usuario provideUsuario() {
        return new Usuario();
    }

    @Provides
    public ModeloStandar provideModeloStandar() { return new ModeloStandar();
    }
    @Provides
    public ServicioInformacionStandar provideServicioInformacionStandar() { return new ServicioInformacionStandar();
    }

    @Provides
    public Estudiante provideEstudiante() { return new Estudiante();
    }

    @Provides
    public ServicioEstudiante provideServicioEstudiante() { return new ServicioEstudiante();
    }


}
