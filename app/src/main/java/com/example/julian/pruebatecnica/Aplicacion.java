package com.example.julian.pruebatecnica;

import android.app.Application;

import com.example.julian.pruebatecnica.Di.Componente;
import com.example.julian.pruebatecnica.Di.DaggerComponente;
import com.example.julian.pruebatecnica.Di.Modulo;

public class Aplicacion extends Application {

    private Componente componente;

    private static Aplicacion app;

    @Override
    public void onCreate() {
        super.onCreate();
        app = this;
        componente = DaggerComponente.builder().modulo(new Modulo(this)).build();
    }

    public Componente getComponente() {
        return componente;
    }

    public static Aplicacion getApp() {
        return app;
    }
}
