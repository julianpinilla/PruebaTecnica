# Prueba Android 

Prueba Tecnica 



### Acceso a la aplicación
* Usuario: root
* Password: root

### MODELO ENTIDAD RELACION

![MER_PRUEBA.png](https://gitlab.com/julianpinilla/PruebaTecnica/blob/master/MER_PRUEBA.png)

## Implementación de la información

Los archivos necesarios para poder poblar los filtros y obtener el usuario y la contraseña se obtienen mediante archivos csv que se encuentran en assets

**Ejemplo**

1;juan;andres;valencia;ramirez;2010-05-05;102030;1;1;1;1;1;''

2;german;andres;velez;ricaurte;2010-05-06;102031;1;2;2;1;2;''

3;alfredo;ricardo;ramirez;alfonso;2010-05-07;102032;1;1;3;1;3;''

4;william;andres;pinilla;perez;2010-05-08;102033;1;2;1;1;4;''

## Estructura
```
Assets:Contiene los archivos de tipo **csv** los cuales poseen la información respectiva para llenar las tablas de la base de datos local

BD: Contiene la conexión a la base de datos y la funcionalidad de registrar la información obtenida en los csv.

DI: Contiene la configuración el inyector de dependencias Dagger.

DI: Contiene la configuración el inyector de dependencias Dagger.

Modelo: Contiene la representación lógica de las tablas creadas.

Presenter: Contiene la funcionalidad principal del app

Servicios: Contiene la interaccion realizadas en la BD para cada uno de las funcionalidades

Utils: Contiene las varios metodos que son usados por toda la APP


## Componentes usados

* SqLite: Motor de Base de datos
* Dagger: inyector de dependecias
* Butter Knife: librería que permite relacionar los elementos de las vistas con el código en nuestras aplicaciones Android
 ##